﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConfLib.Models.Enums
{
    public enum Status : byte
    {
        Deleted = 0,
        Active = 1,
    }
}
